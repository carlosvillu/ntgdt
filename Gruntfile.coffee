module.exports = (grunt) ->
  grunt.initConfig

    clean:
      js: ['instances/site/public/app/js']

    coffee:
      app:
        options:
          sourceMap: true
        expand: true,
        flatten: false,
        cwd: 'instances/site/public/app/src',
        src: ['**/*.coffee'],
        dest: 'instances/site/public/app/js',
        ext: '.js'

    grunt.loadNpmTasks "grunt-contrib-coffee"
    grunt.loadNpmTasks "grunt-contrib-clean"

    grunt.registerTask "default", ["clean:js", "coffee"]
