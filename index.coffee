PORT = process.env.NTGDT_PORT or 3000

express = require 'express'
{Site, Api} = require './instances'
app = express()

#app.use express.logger()

app.use '/api', Api
app.use Site


app.listen PORT, ->
  require('child_process').spawn('grunt')
  console.log "App running in port #{PORT}"
