module.exports =
  db:
    mongo:
      host: '127.0.0.1'
      db: 'ntgdt'
    redis:
      host: '127.0.0.1'
      port: 6379
