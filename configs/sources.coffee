module.exports = origin:
    latam: [
        'chistosasjoryx',
        'naquisimo',
        'imageneschistosaseu',
        'imagenesgraciosas'
    ],
    spain: [
        'cuantodanio',
        'finofilipino',
        'cuantocabron',
        'vistoenredes',
        'tiralatele',
        'vayaface',
        'estamoscenando',
        'quedadicho',
        'failbookes',
        'facebookfails',
        'memedeportes'
    ],
    english: [
        'lolbrary',
        'eatliver',
        'poorlydressed',
        'after12',
        'dating',
        'mondaythrufriday',
        'parenting',
        'thereifixedit',
        'ugliesttattoos',
        'failblog',
        'autocowrecks',
        'failbook'
    ],
    spain_safe: [
        'vistoenredes',
        'tiralatele',
        'quedadicho',
        'failbookes',
        'facebookfails',
        'memedeportes'
    ]
