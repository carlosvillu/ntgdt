module.exports =
  sources: [
    {
      name: 'finofilipino'
      url: 'http://finofilipino.org'
      query_page: '.posts .post-footer a[title="Enlace a la publicación"]'
      query_next: '#content-wrapper .next'
    },
    {
      name: 'cuantodanio'
      url: 'http://cuantodanio.es'
      query_page: '.articles h2 a'
      query_next: '.pagination .next'
    },
    {
      name: 'cuantocabron'
      url: 'http://www.cuantocabron.com/'
      query_page: '.box.story.rounded3px h2 a'
      query_next: '.pager li a[title="Siguiente"]'
    },
    {
      name: 'vistoenredes'
      url: 'http://www.vistoenlasredes.com/'
      query_page: '.box.story h2 a'
      query_next: '.pager li a[title="Siguiente"]'
    },
    {
      name: 'tiralatele'
      url: 'http://www.tiralatele.com'
      query_page: '.infoPost .infVisit a'
      selector_next: '$(".pagination").children().last().attr("href")'
    },
    {
        name: 'vayaface'
        url: 'http://www.vayaface.com/'
        query_page: 'h3.post-title a'
        query_next: '#blog-pager-older-link a'
    },
    {
        name: 'estamoscenando'
        url: 'http://estamoscenando.blogspot.com.es/'
        query_page: 'h3.post-title a'
        query_next: '#blog-pager-older-link a'
    },
    {
      name: 'imagenesgraciosas'
      url: 'http://www.imagenesgraciosas.name/'
      query_page: '.cat_cover_box a.banner'
      selector_next: '$(".pagination li.current").next("li").find("a").attr("href")'
    },
    {
      name: 'chistosasjoryx'
      url: 'http://chistosas.joryx.com/'
      query_page: '#list article a'
      query_next: 'a.siguientepag'
    },
    {
        name: 'naquisimo'
        url: 'http://www.naquisimo.com/category/imagenes-chistosas/'
        query_page: '.post h2 a'
        query_next: 'a.nextpostslink'
    },
    {
        name: 'imageneschistosas'
        url: 'http://www.imageneschistosas.com.mx'
        query_page: '.thumb_grid a'
    },
    {
        name: 'lasfotosmasgraciosas'
        url: 'http://www.lasfotosmasgraciosas.com'
        query_page: '.items article.item .rightcontent a'
        query_next: '.pagination a.next'
    },
    {
        name: 'imageneschistosaseu'
        url: 'http://www.imageneschistosas.eu/'
        selector_page: '$(".object a img").parent()'
        query_next: '.pagination a.next'
    },
    {
        name: 'lolbrary'
        url: 'http://www.lolbrary.com/'
        selector_page: '$("div.post a img").parent()'
        query_next: 'div.big a[rel="next"]'
    },
    {
        name: 'eatliver'
        url: 'http://www.eatliver.com/'
        query_next: '.wp-pagenavi a.next'
        query_page: 'h2.post-title a'
    },
    {
        name: 'poorlydressed'
        url: 'http://failblog.cheezburger.com/poorlydressed'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'after12'
        url: 'http://failblog.cheezburger.com/after12'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'dating'
        url: 'http://failblog.cheezburger.com/dating'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'mondaythrufriday'
        url: 'http://failblog.cheezburger.com/work'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'parenting'
        url: 'http://failblog.cheezburger.com/parenting'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'thereifixedit'
        url: 'http://failblog.cheezburger.com/thereifixedit'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'ugliesttattoos'
        url: 'http://failblog.cheezburger.com/ugliesttattoos'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'failblog'
        url: 'http://failblog.cheezburger.com/fails'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'autocowrecks'
        url: 'http://failblog.cheezburger.com/autocowrecks'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'failbook'
        url: 'http://failblog.cheezburger.com/failbook'
        query_next: 'div.pagination a.icons-alt'
        query_page: 'div.post-header h2 a'
    },
    {
        name: 'quedadicho'
        url: 'http://www.quedadicho.com/'
        query_next: 'div.wp-pagenavi a.nextpostslink'
        query_page: '#posts h2 a'
    },
    {
        name: 'failbookes'
        url: 'http://www.failbook.es/'
        query_next: 'div.wp-pagenavi a.nextpostslink'
        query_page: '#posts h2 a'
    },
    {
        name: 'facebookfails'
        url: 'http://facebookfails.es/'
        query_next: 'div.pagination div.alignleft a'
        query_page: 'h2.title a'
    },
    {
       name: 'memedeportes'
       url: 'http://www.memedeportes.com/'
       query_next: 'ul.pager a[title="Siguiente"]'
       query_page: 'div.box_content h2 a'
    },
    {
       name: 'menudabroma'
       url: 'http://www.menudabroma.com/search?max-results=100'
       selector_next: null
       query_page: '.post-title  a'
    }
  ],
  crop:
    finofilipino: {top: 150},
    cuantodanio: {top: 720},
    cuantocabron: {top: 420},
    vistoenredes: {top: 235},
  scrapper:
    cuantodanio:
      images: '.ob-cell.ob-img.ob-media'
      title: '.bloc-text h2'
    finofilipino:
      images: '.image img'
      title: '.caption'
    cuantocabron:
      images: '.story_content img'
      title: '.box.story.rounded3px h2'
    vistoenredes:
      images: '.story_content img'
      title: '.box.story h2'
    tiralatele:
      images: '.contePost img'
      title: '.caption p'
    vayaface:
        images: 'div.post-body img'
        title: 'h3.post-title'
    estamoscenando:
        images: 'div.ob-section-images img'
        title: 'h3.post-title'
    imagenesgraciosas:
      images: '.coverimage img'
      title: '.page_headline h1'
    chistosasjoryx:
      images: '.imagenp img'
      title: ''
    naquisimo:
      images: '.entry-content img'
      title: 'h1.entry-title'
    imageneschistosas:
      images: '.wallpaper_grid img'
      title: '.headcontenidoverde h1'
    lasfotosmasgraciosas:
      images: 'article.item .rightcontent img'
      title: 'article.item h1.title'
    imageneschistosaseu:
      images: '.object img'
      title: ''
    lolbrary:
      images: 'div.body img'
      title: 'div.headline h1'
    eatliver:
      images: 'div.post-content img'
      title: 'h1.post-title'
    poorlydressed:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    after12:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    dating:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    mondaythrufriday:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    parenting:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    thereifixedit:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    ugliesttattoos:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    failblog:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    autocowrecks:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    failbook:
      title: 'h1.title'
      images: 'div.post-asset-inner img'
    quedadicho:
        title: '#poststitulo h2 a'
        images: '#posts p img'
    failbookes:
        title: '#poststitulo h2 a'
        images: '#posts p img'
    facebookfails:
        title: 'div.content a.bubble'
        images: 'div.content p img'
    memedeportes:
        title: 'div.box_content h2 a'
        images: 'div.img_cont img'
    menudabroma:
        title: '.post-title.entry-title'
        images: '.post-body.entry-content img'
