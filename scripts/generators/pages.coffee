#!/usr/bin/env coffee

##
# Dada una fuente de datos, generalmente un blog, creamos los trabajos sobre las páginas para que sean creados los screenshots
# TODO: PERO QUE MIERDA ES ESTO DE QUE LOS SCRIPTS ATAQUEN DIRECTAMENTE A LA BBDD !!!!!
#
# --debug Muestra los documentos que se están creando
# --limit Cuantas páginas en total se han de salvar en BBDD antes de cerrar el proceso. Default 100
##

async = require 'async'
scrap = require 'scrap'
mongojs = require 'mongojs'
url_parser = require 'url'
{sources} = require '../configs/sources'
argv = require('optimist').argv

totalPagesToSave = argv.limit or 1600
currentPageSaved = 0

env = process.env.NODE_ENV or 'development'
config = require "../../configs/#{env}"
db = mongojs("#{config.db.mongo.host}/#{config.db.mongo.db}", ['pages'])

getlocationurl = (url, location) ->
    host_parsed = url_parser.parse(url)

    location = location.replace host_parsed.pathname, '' if host_parsed?.pathname and '/' != host_parsed.pathname
    location = location.replace url, ''

    return location

scrap_main_page = (url, location, source) ->

  return true if argv.source and argv.source != source.name

  do (url, location, source) ->
    location = getlocationurl(url, location)
    console.log("Parsing #{url}#{location}") if argv.debug

    scrap "#{url}#{location}", (err, $) ->
      process.exit(1) if err
      pages = if source.query_page then $(source.query_page) else eval(source.selector_page)

      pagesToBeSave = pages.map (i, a) ->
        (cb) ->
          db.pages.findOne {url: $(a).attr('href')}, (err, doc) ->
            unless doc
              db.pages.save {url: $(a).attr('href'), random: Math.random(), source: source.name, date: Date.now(), screenshots:false}, cb
            else
              cb null, "Conflicto con #{$(a).attr('href')}"

      async.parallel pagesToBeSave, (err, docs) ->

        console.log docs if argv.debug
        currentPageSaved += docs.length

        if currentPageSaved < totalPagesToSave
          if source.query_next
            scrap_main_page(url, $(source.query_next).attr('href'), source)
          else if source.selector_next
            scrap_main_page(url, eval(source.selector_next), source)
          else
            console.log("No next page") if argv.debug
            process.exit(0)
        else
          console.log("Pages saved #{currentPageSaved}") if argv.debug
          process.exit(0)

sources.forEach (source, index) -> scrap_main_page source.url, '/', source
