##
# casper screenshot.coffee url id ua name height width 
##
casper = require("casper").create({ verbose: true, logLevel: "info"})
[url, id, top, dest] = casper.cli.args


viewports = [{name: name, height: 600, width: 1020}]
casper.start url, ->
  this.echo('Current location is ' + this.getCurrentUrl(), 'info')
 
casper.each viewports, (casper, viewport) ->
  this.then ->
    this.viewport(viewport.width, viewport.height)
  this.then ->
    this.zoom 1
  this.thenOpen url, ->
    this.wait(5000)
  this.then ->
    this.echo "#{dest}/#{id}.jpg", 'info'
    this.capture "#{dest}/#{id}.jpg",
      {top: top, left: 0, width: viewport.width, height: viewport.height},
      {format: 'jpg', quality: 50}

casper.run()
