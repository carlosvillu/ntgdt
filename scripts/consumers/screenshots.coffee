#!/usr/bin/env coffee

##
# Consume de la cola de trabajos y va haciendo los screenshots de las páginas
#
#   --source Solo crea las screenshot sobre una fuente determinada
##
script = "#{__dirname}/bin/screenshots.coffee"

env = process.env.NODE_ENV or 'development'
config = require "../../configs/#{env}"
{crop} = require '../configs/sources'
mongojs = require 'mongojs'
db = mongojs("#{config.db.mongo.host}/#{config.db.mongo.db}", ['pages'])
async = require 'async'
{execFile} = require 'child_process'
argv = require('optimist').argv

takeScreenshot = (opts, cb) ->
  {doc, top} = opts
  dest = "#{__dirname}/../../instances/site/public/screenshots"
  capture = execFile "#{__dirname}/../../node_modules/casperjs/bin/casperjs", ["#{__dirname}/bin/screenshots.coffee", doc.url, doc._id, top, dest], ->
    doc.screenshots = true
    db.pages.update {_id: doc._id}, doc, cb
  capture.stdout.pipe(process.stdout)


queue = async.queue takeScreenshot, 1
queue.drain = -> process.exit(0)

query =
  screenshots: false
if argv.source then query.source = argv.source

db.pages.find query, (err, docs) ->
  if docs
    docs.forEach (doc, index) ->
      queue.push {doc:doc, top: crop[doc.source]?.top or 0}
  else
    process.exit(0)
