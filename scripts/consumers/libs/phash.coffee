env = process.env.NODE_ENV or 'development'
config = require "../../../configs/#{env}"
mongojs = require 'mongojs'
path = require( 'path' )
async = require 'async'
phash = require( 'phash' )
fs = require 'fs'
spawn = require('child_process').spawn
domain = require('domain').create()
pages= mongojs( "#{config.db.mongo.host}/#{config.db.mongo.db}" ).collection('pages')


DELTA = 4
EXACT_MATCH = 0

query =
  $and:[
    { phash: {$exists: true} }
    { phash: {$ne: '0'} }
  ],
  image:{ $exists: true}
  hash:{ $exists: true}

module.exports = ( doc, cb ) ->
  img = '/tmp/' + doc.hash + path.extname( doc.image )
  curl = spawn( 'curl', ['-L', doc.image, '-o', img] )
  imgPhash = null
  match = []

  curl.on 'close', ->
    domain.run ->
      imgPhash = phash.imageHashSync img
      try
        fs.unlinkSync( img )
      catch ex
        console.log 'Error al borrar la imagen:', img

      pages.find( query, {_id: 1, phash: 1} ).sort {date: -1}, ( err, pages ) ->
        pages.forEach (page) ->
          match.push(page) if phash.hammingDistance(imgPhash, page.phash) is EXACT_MATCH
        if 3 > match.length > 0 then cb( new Error 'exact match' ) else cb( null, imgPhash )
