#!/usr/bin/env coffee

md5 = require('MD5')
fs = require 'fs'
http = require 'http'
https = require 'https'
scrap = require 'scrap'
env = process.env.NODE_ENV or 'development'
config = require "../../configs/#{env}"
{scrapper} = require '../configs/sources'
mongojs = require 'mongojs'
db = mongojs("#{config.db.mongo.host}/#{config.db.mongo.db}", ['pages'])
async = require 'async'
{execFile} = require 'child_process'
argv = require('optimist').argv
url = require 'url'
crypto = require 'crypto'
phash = require './libs/phash'


scrapUrl = (opts, cb) ->
  {doc,rules} = opts
  ##console.log doc, rules if argv.debug
  scrap doc.url, (err, $) ->
    title = $(rules.title)?.text().trim()
    image = $(rules.images)?.attr('src')

    doc.review = true
    if argv.debug
      console.log """
                    \n
                    #{title}
                    #{image}
                    #{doc.url}
                    -------------------------------
                  """
    if image

      if 'http' != image.substr( 0, 4 )
        url_parsed = url.parse( doc.url )
        image = "#{url_parsed.protocol}//#{url_parsed.host}#{image}"

      doc.title = title if title
      doc.image = image

      console.log "Process imagen #{doc.image}" if argv.debug
      protocolo = if url.parse(doc.image).protocol is 'http:' then http else https
      req = protocolo.get doc.image, (res) ->
        console.log "Descargado imagen #{doc.image}" if argv.debug
        hash = crypto.createHash('sha1')
        hash.setEncoding('hex')
        res.pipe(hash)
        res.on 'end', ->
          hash.end()
          file_hash = hash.read()
          console.log 'Imagen hash: ', file_hash if argv.debug
          db.pages.findOne {hash: file_hash}, (err, doc_found) ->
            unless doc_found
              doc.hash = file_hash
              phash doc, (err, phash) ->
                unless err
                  doc.phash = phash
                  console.log "phash #{phash}"
                  db.pages.update {_id: doc._id}, doc, (err, doc) ->
                    #console.log doc if argv.debug
                    cb()
                else
                  console.log "Imagen Similar"
                  db.pages.remove {_id: doc._id}, ->
                    console.log "Pagina eliminada", doc._id
                    cb()
            else
              console.log "Imagen Duplicada!" if argv.debug
              db.pages.remove {_id: doc._id}, ->
                console.log "Pagina eliminada", doc._id
                cb()
    else
      db.pages.update {_id: doc._id}, doc, (err, doc) ->
        console.log "Pasando! #{image}" if argv.debug
        cb()

queue = async.queue scrapUrl, 1
queue.drain = -> process.exit(0)

query =
  image: { $exists: false }
  review: { $exists: false }
if argv.source then query.source = argv.source

db.pages.find query, (err, docs) ->
  if docs.length
    docs.forEach (doc, index) ->
      if scrapper[doc.source]
        queue.push {doc: doc, rules: scrapper[doc.source]}
  else
    process.exit(0)
