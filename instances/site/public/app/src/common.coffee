require.config
  baseUrl: '/app/js'
  paths:
    jquery: "/components/jquery/dist/jquery.min"
    underscore: "/components/underscore/underscore"
    backbone: "/components/backbone/backbone"
    bootstrap: "/components/bootstrap/dist/js/bootstrap.min"
    jade: "/components/jade/runtime"
    supersized: "/libs/supersized/js/supersized.core.3.2.1.min"
    mousetrap: "/libs/mousetrap/mousetrap.min"
    backstretch: "/components/jquery-backstretch/jquery.backstretch.min"
    hammerjs: "/components/hammerjs/hammer.min"
    framework7: "/components/Framework7/dist/js/framework7.min"
    swiper: "/components/swiper/dist/idangerous.swiper.min"
    blazy:"/libs/blazy/blazy.min"
  shim:
    backbone:
      deps: ["jquery", "underscore"],
      exports: "Backbone"
    underscore:
      exports: '_'
    bootstrap:
      deps: ["jquery"]
    supersized:
      deps: ["jquery"]
    mousetrap:
      deps: ["jquery"]
    backstretch:
      deps: ["jquery"]
    hammerjs:
      deps: ["jquery"]
    framework7:
      exports: "Framework7"
    swiper:
      deps:["jquery"]
  waitSeconds: 10
  
define ["backbone", "swiper", "framework7", "jade"], ->
  window.App = new window.Framework7()
  mainView = window.App.addView '.view-main', dynamicNavbar: true
