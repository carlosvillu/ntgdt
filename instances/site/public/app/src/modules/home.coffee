define (require) ->
  HomeView =  require 'views/home'
  PagesCollection = require 'collections/pages'
  
  pageCollection = new PagesCollection
    random: parseInt( $('meta[name="random"]', 'head').attr('content'), 10 ) is 1
    page: parseInt( $('meta[name="page"]', 'head').attr('content'), 10 )

  homeView = new HomeView
    el: document.getElementById 'timeline'
    collection: pageCollection
