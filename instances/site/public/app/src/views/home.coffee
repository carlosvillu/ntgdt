define (require) ->
  Swiper = require 'swiper'
  class HomeView extends Backbone.View
    events:
      "click #more": ->
        window.App.showIndicator 'Monos trabajando ...'
        @collection.next()
    initialize: (opts) ->
      @collection.on 'reset', @onFetch, this

    onFetch: (collection) ->
      collection.each (doc) ->
        document.getElementById('pics').innerHTML += """
                                                        <div class='pic'>
                                                          <div class='row'>
                                                            <div class='col-100'>
                                                              <h2 class='truncate'>
                                                                #{doc.get('title')}
                                                              </h2>
                                                            </div>
                                                          </div>
                                                          <div class='row'>
                                                            <div class='col-100'>
                                                              <a href='/image?id=#{doc.get('_id')}'>
                                                                <img class='responsive' src='#{doc.image()}'/> 
                                                              </a>
                                                            </div>
                                                          </div>
                                                        </div>
                                                     """
      window.App.hideIndicator()
