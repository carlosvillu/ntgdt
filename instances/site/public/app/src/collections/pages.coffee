define (require) ->
  
  PageModel = require 'models/page'
  class PagesCollection extends Backbone.Collection
    url: ->
      random_slug = if @random then '/random' else ''
      "/api/pages#{random_slug}"
    model: PageModel
    initialize: (opts) ->
      {@page, @random} = opts
    next: ->
      @fetch {reset: yes, data: {page: ++@page}}
    images: ->
      @map (model) -> model.image()
