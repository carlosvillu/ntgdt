PORT = process.env.NTGDT_PORT or 3000
express = require 'express'
request = require 'request'
thumbs = require 'connect-thumbs'
app = express()

Home = require './controllers/home'
Image = require './controllers/image'

app.set 'views', "#{__dirname}/views"
app.set 'view engine', 'jade'

# Thumb on the fly!!
app.use thumbs
  decodeFn: (id, cb) ->
    request.get "http://localhost:#{PORT}/api/pages/#{id}", {json: true}, (err, resp, body) ->
      monos = "http://1.bp.blogspot.com/_V7q0Kr-kpqQ/TNgNmoEQNCI/AAAAAAAAABo/Q3zsMbYTbgg/s1600/monos+trabajando.jpg"
      url = if body.image.indexOf('.gif') isnt -1 then monos else body.image
      console.log url
      cb(err, url)

app.use express.static("#{__dirname}/public")#, maxAge: 86400000 * 30) # One Month
app.use(express.cookieParser('navidad dulce navidad'))
app.use express.session()

app.get '/', Home.index
app.get '/image', Image.index

module.exports = app
