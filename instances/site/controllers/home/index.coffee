PORT = process.env.NTGDT_PORT or 3000
_ = require 'underscore'
request = require 'request'
module.exports =
  index: (req, res, next) ->
    page = parseInt( req.query.page, 10 ) or 0
    random = parseInt( req.query.random, 10 )
    random_slug = if random is 1 then '/random' else ''
    request.get "http://localhost:#{PORT}/api/pages#{random_slug}?page=#{page}", {json:true}, (err, resp, body) ->
      opts =
        pics: _.reject(body, (pic) -> pic.source is 'cuantocabron')
        page: page
        random: random
      if !page
        opts.id = 'home'
        res.render 'home', opts
      else
        res.render 'home/_partial', opts
