PORT = process.env.NTGDT_PORT or 3000
request = require 'request'
module.exports =
  index: (req, res, next) ->
    request.get "http://localhost:#{PORT}/api/pages/#{req.query.id}", {json: true}, (err, resp, body) ->
      res.render 'image', doc: body
