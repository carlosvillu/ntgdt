express = require 'express'
app = express()

Pages = require './controllers/pages'

app.get '/pages', Pages.all
app.get '/pages/random', Pages.random
app.get '/pages/:id', Pages.one

module.exports = app
