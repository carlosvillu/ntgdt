env = process.env.NODE_ENV or 'development'
config = require "../../../../configs/#{env}"
mongojs = require 'mongojs'
db = mongojs("#{config.db.mongo.host}/#{config.db.mongo.db}", ['pages'])
_ = require 'underscore'
sources = require "../../../../configs/sources"

DOCS_PER_PAGE = 50
DEFAULT_ORIGIN = 'spain'

module.exports =
  all: (req, res, next) ->
    skip = req.query.page or 0
    limit = parseInt(req.query.limit, 10) or DOCS_PER_PAGE
    source = req.query.origin or DEFAULT_ORIGIN
    db.pages.find({image: {$exists: true}, source: {$in: sources.origin[source]}}).limit(limit).skip(skip * limit).sort {date:-1}, (err, docs) ->
      res.json docs

  one: (req, res, next) ->
    db.pages.findOne {_id: mongojs.ObjectId(req.params.id)}, (err, doc) ->
      res.json doc

  random: (req, res, next) ->
    limit = parseInt(req.query.limit, 10) or DOCS_PER_PAGE
    random = Math.random()
    source = req.query.origin or DEFAULT_ORIGIN
    db.pages.find {image: {$exists: true}, random:{$lt: random}, source: {$in: sources.origin[source]}}, (err, docs) ->
      res.send _.shuffle(docs).slice(0, limit)
